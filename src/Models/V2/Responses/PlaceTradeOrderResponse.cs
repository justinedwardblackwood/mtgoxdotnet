﻿namespace Models.V2.Responses
{
	public sealed class PlaceTradeOrderResponse
	{
		public string result { get; set; }
		
		// Contains the order id.
		public string data { get; set; }
	}
}
