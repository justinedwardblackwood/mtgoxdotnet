﻿namespace Models.V2.Responses
{
	public sealed class CancelTradeOrderResponse
	{
		public string result { get; set; }

		public CancelTradeOrderData data { get; set; }
	}

	public sealed class CancelTradeOrderData
	{
		public string oid { get; set; }

		public string qid { get; set; }
}
}
