﻿using System.Collections.Generic;

namespace Models.V2
{
	public class OpenOrders
	{
		public string result { get; set; }

		public List<Offers> data { get; set; }
	}

	public class Offers
	{
		public string oid { get; set; }

		public string currency { get; set; }

		public string item { get; set; }

		public Dictionary<string, object> amount { get; set; }

		public Dictionary<string, object> effective_amount { get; set; }

		public Dictionary<string, object> price { get; set; }

		public string type { get; set; }
		
		public string status { get; set; }

		public long date { get; set; }

		public string priority { get; set; }

		public List<string> actions { get; set; }
	}
}
