﻿using System.Collections.Generic;

namespace Models.V2
{
	public sealed class Streams
	{
		public string result { get; set; }

		public Dictionary<string, string> data { get; set; }

		public string GetTicker(TickerType tickerType)
		{
			string streamAddress = null;

			string ticker = string.Format("ticker.{0}", tickerType);

			if (data.ContainsKey(ticker))
				streamAddress = data[ticker];

			return streamAddress;
		}
	}
}
