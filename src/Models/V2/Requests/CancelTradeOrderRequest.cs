﻿namespace Models.V2.Requests
{
	public sealed class CancelTradeOrderRequest
	{
		public string oid { get; set; }
	}
}
