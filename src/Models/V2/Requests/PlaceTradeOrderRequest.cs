﻿namespace Models.V2.Requests
{
	public sealed class PlaceTradeOrderRequest
	{
		public OrderType type { get; set; }

		// Amount of BTC to buy or sell, as an integer.
		public int amount_int { get; set; }

		// The price per Bitcoin in original currency as an integer. Optional if you wish to place a market order.
		public int? price_int { get; set; }
	}
}
