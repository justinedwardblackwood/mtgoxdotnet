﻿namespace Models.V2
{
	public enum CurrencySymbol
	{
			USD,
			AUD,
			CAD,
			CHF,
			CNY,
			DKK,
			EUR,
			GBP,
			HKD,
			JPY,
			NZD,
			PLN,
			RUB,
			SEK,
			SGD,
			THB,
			BTC
	}
}
