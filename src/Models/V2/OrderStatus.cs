﻿namespace Models.V2
{
	public enum OrderStatus
	{
		Active,
		NotSufficientFunds,
		Pending,
		Invalid
	}
}
