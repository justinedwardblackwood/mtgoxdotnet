﻿namespace Models.V2
{
	public class TickerFast
	{
		public string result { get; set; }

		public AmountsAndNow data { get; set; }

		public class AmountsAndNow
		{
			public Amount last_local { get; set; }

			public Amount last { get; set; }

			public Amount last_orig { get; set; }

			public Amount last_all { get; set; }

			public Amount buy { get; set; }

			public Amount sell { get; set; }

			public string now { get; set; }
		}

		public class Amount
		{
			public decimal value { get; set; }

			public long value_int { get; set; }

			public string display { get; set; }

			public string display_short { get; set; }

			public string currency { get; set; }
		}
	}
}
