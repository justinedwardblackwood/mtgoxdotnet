﻿using Models.V2.Requests;
using Models.V2;
using Models.V2.Responses;

namespace MtGoxClient
{
	public sealed class HttpV2Client
	{
		public HttpV2Client(string apiKey, string apiSecret, string userAgent = null)
		{
			m_apiKey = apiKey;
			m_apiSecret = apiSecret;
			m_userAgent = userAgent;
		}

		public OpenOrders GetOpenOrders()
		{
			const string prefix = "money/orders";

			return RequestUtility.ExecuteRequest<OpenOrders>(c_baseURL, m_version, prefix, m_apiKey, m_apiSecret, AccessType.Private, Method.POST, userAgent: m_userAgent);
		}

		public TickerFast GetTickerFast()
		{
			const string prefix = "BTCUSD/money/ticker_fast";

			return RequestUtility.ExecuteRequest<TickerFast>(c_baseURL, m_version, prefix, m_apiKey, m_apiSecret, AccessType.Public, Method.GET, userAgent: m_userAgent);
		}

		public PlaceTradeOrderResponse PlaceTradeOrder(PlaceTradeOrderRequest currencyTradeOrderRequest)
		{
			const string prefix = "BTCUSD/money/order/add";

			string arguments = string.Format("type={0}&amount_int={1}", currencyTradeOrderRequest.type, currencyTradeOrderRequest.amount_int);

			if (currencyTradeOrderRequest.price_int.HasValue)
				arguments += string.Format("&price_int={0}", currencyTradeOrderRequest.price_int);

			return RequestUtility.ExecuteRequest<PlaceTradeOrderResponse>(c_baseURL, m_version, prefix, m_apiKey, m_apiSecret, AccessType.Private, Method.POST, arguments, m_userAgent);
		}

		public CancelTradeOrderResponse CancelTradeOrder(CancelTradeOrderRequest cancelTradeOrderRequest)
		{
			const string prefix = "money/order/cancel";

			string arguments = string.Format("oid={0}", cancelTradeOrderRequest.oid);

			return RequestUtility.ExecuteRequest<CancelTradeOrderResponse>(c_baseURL, m_version, prefix, m_apiKey, m_apiSecret, AccessType.Private, Method.POST, arguments, m_userAgent);
		}

		public Streams GetPublicStream()
		{
			const string prefix = "stream/list_public";

			return RequestUtility.ExecuteRequest<Streams>(c_baseURL, m_version, prefix, m_apiKey, m_apiSecret, AccessType.Public, Method.GET, userAgent: m_userAgent);
		}

		private const string c_baseURL = "https://data.mtgox.com/api";

		private const string m_version = "2";

		private readonly string m_apiKey;

		private readonly string m_apiSecret;

		private readonly string m_userAgent;
	}
}
