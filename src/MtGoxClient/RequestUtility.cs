﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Models.V2;
using Newtonsoft.Json;

namespace MtGoxClient
{
	public static class RequestUtility
	{
		public static T ExecuteRequest<T>(string baseUrl, string version, string prefix, string apiKey, string apiSecret, AccessType accessType, Method method, string arguments = null, string userAgent = null)
		{
			string url = string.Format("{0}/{1}/{2}", baseUrl, version, prefix);

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
			webRequest.ContentType = "application/x-www-form-urlencoded";
			webRequest.UserAgent = userAgent ?? string.Empty;
			webRequest.Accept = "application/json";

			webRequest.Method = Convert.ToString(method);

			if (accessType == AccessType.Private)
			{
				string postData = string.Format("nonce={0}", DateTime.Now.Ticks);

				// Add nonce value, so that no request can be replayed.  Handled up to 64 bits on MtGox (up to 2^63)
				// The requested path (anything after the /2/) must now be included when you compute your hashed signature (Rest-Sign) for added security.
				string message = string.Format("{0}\0{1}", prefix, postData);

				if (!string.IsNullOrEmpty(arguments))
				{
					string additionalArgs = string.Format("&{0}", arguments);

					message += additionalArgs;
					postData += additionalArgs;
				}

				string restSign = GetHash(apiSecret, message);

				webRequest.Headers["Rest-Key"] = apiKey;

				webRequest.Headers["Rest-Sign"] = restSign;

				byte[] byteArray = Encoding.UTF8.GetBytes(postData);

				webRequest.ContentLength = byteArray.Length;

				using (Stream dataStream = webRequest.GetRequestStream())
				{
					dataStream.Write(byteArray, 0, byteArray.Length);
				}
			}

			string jsonResponse;

			using (WebResponse webResponse = webRequest.GetResponse())
			using (Stream str = webResponse.GetResponseStream())
			using (StreamReader sr = new StreamReader(str))
				jsonResponse = sr.ReadToEnd();

			return JsonConvert.DeserializeObject<T>(jsonResponse);
		}

		// Decode API secret using Base64
		// Perform the HMAC algorithm on your message using SHA-512 for encryption method.
		// Encode the HMAC result using Base64
		// This is the "Rest-Sign"
		private static string GetHash(string apiSecret, string message)
		{
			byte[] secret = Convert.FromBase64String(apiSecret);
			HMACSHA512 hmacsha512 = new HMACSHA512(secret);
			byte[] messageBytes = Encoding.UTF8.GetBytes(message);
			return Convert.ToBase64String(hmacsha512.ComputeHash(messageBytes));
		}
	}
}
