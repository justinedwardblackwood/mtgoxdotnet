MtGoxDotNet
===============

MtGox Client for Http V2.
Written in C# using .NET v4.5

To start using, initialize the HttpV2Client, you will need the apiKey and apiSecret.
These credentials are supplied by MtGox.
The userAgent is optional.

```
HttpV2Client client = new HttpV2Client("apiKey", "apiSecret", "userAgent");
client.GetOpenOrders();
```

Not all routes are implemented.
For more documentation see here:
https://en.bitcoin.it/wiki/MtGox/API/HTTP/v2
